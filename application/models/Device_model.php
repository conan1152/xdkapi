<?php

class Device_model extends CI_Model {

    public function getData() {
        $query = $this->db->query("select * from device_status order by id desc");
        return $query->result();
    }

    public function insertData($data) {
        $data["device_name"] = $data["device"];
        unset($data["device"]);

        $this->db->insert('device_status', $data);
        $data['device_status'] = $this->db->insert_id();
        return $this->onComplete($data);
    }

    public function getCurrent() {
        $query = $this->db->query("select * from  device_status where id in (select max(id) from device_status group by device_name)");
        return $query->result();
    }

    public function getDataRange($array) {
        $query = $this->db->query("select * from  device_status where created_date >= ? and created_date < ? order by id", array($array["sdate"], $array["edate"]));
        return $query->result();
    }

}
