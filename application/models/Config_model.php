<?php

class Config_model extends CI_Model {

    public function getData() {
        $query = $this->db->query("select * from config ");
        return $query->result();
    }

    public function insertData($data) {
        $data["createdby"] = $this->session->tbluser_id;
        $data["deleted"] = false;
        $data["createddate"] = "now()";
        $this->db->insert('config', $data);
        $data['id'] = $this->db->insert_id();
        return $this->onComplete($data);
    }

    public function updateData($data) {
        $this->db->where('id', $data["id"]);
        $this->db->update('config', $data);
        return $this->onComplete($data);
    }



}
