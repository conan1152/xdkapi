<?php

class Bank_model extends CI_Model {

    public function getData() {
        $query = $this->db->query("select * from tblbank where deleted = false order by code");
        return $query->result();
    }

    public function insertData($data) {
        $data["createdby"] = $this->session->tbluser_id;
        $data["deleted"] = false;
        $data["createddate"] = "now()";
        $this->db->insert('tblbank', $data);
        $data['tblbank_id'] = $this->db->insert_id();
        return $this->onComplete($data);
    }

    public function updateData($data) {

        if (!isset($data["tblbank_id"])) {
            return $this->insertData($data);
        }
        
        $data["updateddate"] = "now()";
        $data["updatedby"] = $this->session->tbluser_id;
        $this->db->where('tblbank_id', $data["tblbank_id"]);
        $this->db->update('tblbank', $data);
        return $this->onComplete($data);
    }

    public function deleteData($data) {
        $data["deleted"] = true;
        $data["deleteddate"] = "now()";
        $data["deletedby"] = $this->session->tbluser_id;
        $this->db->where('tblbank_id', $data["tblbank_id"]);
        $this->db->update('tblbank', $data);
        return $this->onComplete($data);
    }

}
