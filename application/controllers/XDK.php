<?php

class XDK extends CI_Controller {

    public function getData() {
        $this->load->model('Device_model');
        echo json_encode($this->Device_model->getData());
    }

    public function insertData() {
        $this->load->model('Device_model');
        $data = json_decode(file_get_contents('php://input'));
        $array = json_decode(json_encode($data), True);
        echo json_encode($this->Device_model->insertData($array));

        $this->load->model('Config_model');
        $configs = $this->Config_model->getData();
        $config = $configs[0];
        if ($config->warn_temp <= ($data->temp / 1000)) {
            $this->callLineAPI("แจ้งเตือนอุณหภูมิเกินกำหนด");
        }
        if ($config->warn_noise <= ($data->noise * 10000)) {
            $this->callLineAPI("แจ้งเตือนอุณหภูมิเกินกำหนด");
        }

        if ($config->warn_humidity <= $data->humidity) {
            $this->callLineAPI("แจ้งเตือนความชื้นเกินกำหนด");
        }
    }

    public function getCurrent() {
        $this->load->model('Device_model');
        echo json_encode($this->Device_model->getCurrent());
    }

    public function getDataRange() {
        $this->load->model('Device_model');
        $data = json_decode(file_get_contents('php://input'));
        $array = json_decode(json_encode($data), True);
        echo json_encode($this->Device_model->getDataRange($array));
    }

    public function callLineAPI($msg) {


        $url = 'https://api.line.me/v2/bot/message/push';

        $data["to"] = "Cd39ae28f900842d0c793d776b741abba";

        //$message["type"] = "text";
        //$message["text"] = "แจ้งเตือนอุณหภูมิเกินกำหนด";
        //$data["messages"] = array();
        //$data["messages"][] = $message;

        $message["type"] = "text";
        $message["text"] = $msg;
        $data["messages"][] = $message;

        $options = array(
            'http' => array(
                'header' => "Content-type: application/json\r\nAuthorization: Bearer xd6gLjsCF1uqITSDEerovEhd2f73wj+2ui0NE89S9bYmhv5hrgfodtCoSxfu2eeDNg728MQFSxfL2Do1chz3f9EgtVhMbUQnCAVGHpsoiWHNVneAO+KwZu9eob/ZYnWbkUeM0hmB9aMRsdrTEiu0twdB04t89/1O/w1cDnyilFU=",
                'method' => 'POST',
                'content' => json_encode($data),
            )
        );

        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        var_dump($result);



        //$this->render("empty");
    }

}
