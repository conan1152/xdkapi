<?php

class Config extends CI_Controller {

    public function getData() {
        $this->load->model('Config_model');
        echo json_encode($this->Config_model->getData());
    }

    public function editData() {
        $this->load->model('Config_model');

        $data = json_decode(file_get_contents('php://input'));
        $array = json_decode(json_encode($data), True);

        echo json_encode($this->Config_model->updateData($array));
    }

}
