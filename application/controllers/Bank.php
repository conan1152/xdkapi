<?php

class Bank extends CI_Controller {

    public function getData() {
        $this->load->model('Bank_model');
        echo json_encode($this->Bank_model->getData());
    }

    public function insertData() {
        $this->load->model('Bank_model');

        $data = json_decode(file_get_contents('php://input'));
        $array = json_decode(json_encode($data), True);

        echo json_encode($this->Bank_model->insertData($array));
    }

    public function editData() {
        $this->load->model('Bank_model');

        $data = json_decode(file_get_contents('php://input'));
        $array = json_decode(json_encode($data), True);

        echo json_encode($this->Bank_model->updateData($array));
    }

    public function deleteData() {
        $this->load->model('Bank_model');

        $data = json_decode(file_get_contents('php://input'));
        $array = json_decode(json_encode($data), True);

        echo json_encode($this->Bank_model->deleteData($array));
    }

}
